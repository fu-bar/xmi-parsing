package code.blocks.enums;

public enum CollectionType {
    NONE,
    LIST,
    SET
}
