package code.blocks;


import code.blocks.enums.CollectionType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FieldInfo {
    private String name;
    private String fieldType;
    private CollectionType collectionType;
    private final Map<String, String> detailedAnnotations = new HashMap<>();
    private final List<String> fieldAnnotations = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public Map<String, String> getDetailedAnnotations() {
        return detailedAnnotations;
    }

    public List<String> getFieldAnnotations() {
        return fieldAnnotations;
    }

    public CollectionType getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(CollectionType collectionType) {
        this.collectionType = collectionType;
    }
}
