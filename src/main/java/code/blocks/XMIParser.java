package code.blocks;


import code.blocks.enums.CodeType;
import code.blocks.enums.CollectionType;
import code.exceptions.GenException;
import code.utils.Serialization;
import code.utils.XMLHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

import static code.blocks.constants.Xpath.*;

public class XMIParser {

    private final Logger log = LogManager.getLogger(XMIParser.class);
    private final XMLHelper xmlHelper = new XMLHelper();
    private final Map<String, TypeInfo> idToInfo = new HashMap<>();

    public List<TypeInfo> parseXML(String xmiFilePath) {

        validateFile(xmiFilePath);
        log.info(String.format("Parsing file: %s", xmiFilePath));

        Document document = xmlHelper.readFile(xmiFilePath);
        List<Node> classes = xmlHelper.fetchXpath(document, CLASSES);
        List<Node> dataTypes = xmlHelper.fetchXpath(document, DATA_TYPES);
        log.info(String.format("Collected %d classes and %d data-types", classes.size(), dataTypes.size()));


        List<Node> classExtensions = xmlHelper.fetchXpath(document, CLASS_EXTENSIONS);
        List<Node> dataTypeExtensions = xmlHelper.fetchXpath(document, DATATYPE_EXTENSIONS);
        log.info(String.format("Collected %d class-extensions and %d data-type extensions", classExtensions.size(), dataTypeExtensions.size()));

        return readInfo(classes, dataTypes, classExtensions).stream()
                .filter(typeInfo -> typeInfo.getCodeType().equals(CodeType.CLASS))
                .collect(Collectors.toList());
    }

    private void validateFile(String xmiFilePath) {
        File file = new File(xmiFilePath);
        if (!file.exists() || file.isDirectory()) {
            String errorMessage = String.format("Not a file: %s", xmiFilePath);
            log.error(errorMessage);
            throw new GenException(errorMessage);
        }
    }

    private List<TypeInfo> readInfo(List<Node> classes, List<Node> dataTypes, List<Node> classExtensions) {
        log.debug(String.format("Processing: %d classes, %d data-types, %d class-extensions",
                classes.size(), dataTypes.size(), classExtensions.size()));
        idToInfo.clear();
        readInitialInfo(classes, dataTypes);
        checkForInheritance(classes);
        readFields(classExtensions);
        ArrayList<TypeInfo> baseForCodeGeneration = new ArrayList<>(idToInfo.values());
        log.trace(String.format("Base for code generation: %s", Serialization.toJson(baseForCodeGeneration)));
        return baseForCodeGeneration;
    }

    private void readInitialInfo(List<Node> classes, List<Node> dataTypes) {
        Arrays.asList(classes, dataTypes).forEach(
                collection -> collection.forEach(
                        node -> {
                            TypeInfo typeInfo = new TypeInfo(node);
                            String type = xmlHelper.getAttribute(node, XMI_TYPE_ATTRIBUTE);
                            if (!SUPPORTED_ELEMENTS.contains(type)) {
                                String errorMessage = String.format("Element type is not supported: %s (must be one of: %s)", type, SUPPORTED_ELEMENTS);
                                log.error(errorMessage);
                                throw new GenException(errorMessage);
                            }
                            typeInfo.setCodeType(type.equals(UML_CLASS) ? CodeType.CLASS : CodeType.DATA_TYPE);

                            typeInfo.setName(xmlHelper.getAttribute(node, NAME_ATTRIBUTE));
                            typeInfo.setId(xmlHelper.getAttribute(node, XMI_ID_ATTRIBUTE));

                            validateNodeId(typeInfo.getName(), typeInfo.getId());
                            idToInfo.put(typeInfo.getId(), typeInfo);
                            log.trace(String.format("[BASIC]: %s", Serialization.toJson(typeInfo)));
                        }
                ));
    }

    private void readFields(List<Node> classExtensions) {
        classExtensions.forEach(node -> {

            String referencedId = xmlHelper.getAttribute(node, XMI_ID_REF_ATTRIBUTE);
            TypeInfo typeInfo;
            if (idToInfo.containsKey(referencedId)) {
                typeInfo = idToInfo.get(referencedId);
            } else {
                return;
            }
            List<Node> attributes = xmlHelper.fetchXpath(node, CLASS_ATTRIBUTES);

            TypeInfo finalTypeInfo = typeInfo;

            attributes.forEach(attribute -> {
                FieldInfo fieldInfo = new FieldInfo();
                fieldInfo.setName(xmlHelper.getAttribute(attribute, NAME_ATTRIBUTE));
                Node properties = xmlHelper.fetchXpath(attribute, "properties").get(0);
                fieldInfo.setFieldType(xmlHelper.getAttribute(properties, "type"));
                String collection = xmlHelper.getAttribute(properties, "collection");
                if (collection.equals(FALSE)) {
                    fieldInfo.setCollectionType(CollectionType.NONE);
                } else {
                    String duplicates = xmlHelper.getAttribute(properties, "duplicates");
                    fieldInfo.setCollectionType(duplicates.equals(FALSE) ? CollectionType.SET : CollectionType.LIST);
                }
                List<Node> xrefs = xmlHelper.fetchXpath(attribute, "xrefs");
                if (!xrefs.isEmpty() && xrefs.get(0).hasAttributes()) {
                    Node refsNode = xrefs.get(0);
                    String xrefsValue = xmlHelper.getAttribute(refsNode, "value");
                    parseXrefs(fieldInfo, xrefsValue);
                }

                List<Node> tags = xmlHelper.fetchXpath(attribute, "tags/tag");
                tags.forEach(tag -> fieldInfo.getDetailedAnnotations().put(xmlHelper.getAttribute(tag, NAME_ATTRIBUTE), xmlHelper.getAttribute(tag, "value")));

                finalTypeInfo.getFieldInfos().add(fieldInfo);
            });
            log.trace(String.format("[WITH-FIELDS]: %s", Serialization.toJson(finalTypeInfo)));
        });
    }

    private void checkForInheritance(List<Node> classes) {
        log.debug("Checking for inheritance");
        classes.forEach(node -> {
            List<Node> inheritsFrom = xmlHelper.fetchXpath(node, GENERALIZATION_ATTRIBUTE);
            if (!inheritsFrom.isEmpty()) {
                // Assuming a single inheritance (JAVA)
                String currentId = xmlHelper.getAttribute(node, XMI_ID_ATTRIBUTE);
                String parentId = xmlHelper.getAttribute(inheritsFrom.get(0), GENERAL_ATTRIBUTE);
                String parentClassName = idToInfo.get(parentId).getName();
                idToInfo.get(currentId).setInherits(parentClassName);
                log.info(String.format("[Inheritance] %s inherits from %s", idToInfo.get(currentId).getName(), parentClassName));
            }
        });
    }

    private void parseXrefs(FieldInfo fieldInfo, String xrefs) {
        Matcher matcher = STEREOTYPE_REGEX.matcher(xrefs);
        while (matcher.find()) {
            fieldInfo.getFieldAnnotations().add(matcher.group(1));
        }
    }

    private void validateNodeId(String nodeName, String id) {
        if (idToInfo.containsKey(id)) {
            throw new GenException(String.format("Already dealt with id of %s : %s", nodeName, id));
        }
    }
}
