package code.config;

import java.util.List;
import java.util.Map;

public class Config {
    private List<String> firstPriorityPackages;

    private List<String> generalPackages;

    private Map<String, String> typeToPackageTranslation;

    public List<String> getFirstPriorityPackages() {
        return firstPriorityPackages;
    }

    public void setFirstPriorityPackages(List<String> firstPriorityPackages) {
        this.firstPriorityPackages = firstPriorityPackages;
    }

    public List<String> getGeneralPackages() {
        return generalPackages;
    }

    public void setGeneralPackages(List<String> generalPackages) {
        this.generalPackages = generalPackages;
    }

    public Map<String, String> getTypeToPackageTranslation() {
        return typeToPackageTranslation;
    }

    public void setTypeToPackageTranslation(Map<String, String> typeToPackageTranslation) {
        this.typeToPackageTranslation = typeToPackageTranslation;
    }
}
