package code.utils;

import code.config.Config;
import code.exceptions.GenException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TypeResolver {
    private final Logger log = LogManager.getLogger(TypeResolver.class);
    private final Config config;

    public TypeResolver(Config config) {
        this.config = config;
    }

    public Class<?> resolve(String clazz) {

        if (clazz.equals("int")) {
            return int.class;
        }

        if (config.getTypeToPackageTranslation().containsKey(clazz)) {
            try {
                return Class.forName(config.getTypeToPackageTranslation().get(clazz));
            } catch (ClassNotFoundException e) {
                log.error(e.getMessage(), e);
                throw new GenException(e.getMessage());
            }
        }

        List<String> packages = Stream.concat(
                config.getFirstPriorityPackages().stream(),
                config.getGeneralPackages().stream()
        ).collect(Collectors.toList());

        for (String packageName : packages) {
            try {
                return Class.forName(String.format("%s.%s", packageName, clazz));
            } catch (ClassNotFoundException e) {
                // That's fine
            }
        }

        String errorMessage = String.format("Failed resolving type for: %s", clazz);
        this.log.error(errorMessage);
        throw new GenException(errorMessage);
    }
}
