package code.utils;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommandLineHelper {
    private static final Logger log = LogManager.getLogger(CommandLineHelper.class);

    private CommandLineHelper() {
        // Static class
    }

    public static org.apache.commons.cli.CommandLine parseCommandLine(String[] args, Options options) {
        Option option = Option.builder("h")
                .longOpt("help")
                .desc("Show this help message")
                .required(false)
                .build();
        options.addOption(option);
        option = Option.builder("pkg")
                .longOpt("packageFQN")
                .desc("The package to be generated (for example: osf.my.package)")
                .required(true)
                .hasArg(true)
                .build();
        options.addOption(option);
        option = Option.builder("xf")
                .longOpt("xmiFilePath")
                .desc("Input XMI (xml) file to be used for code generation")
                .required(true)
                .hasArg(true)
                .build();
        options.addOption(option);
        option = Option.builder("tf")
                .longOpt("targetFolder")
                .desc("The folder under which the code package is to be generated")
                .required()
                .hasArg(true)
                .build();
        options.addOption(option);
        option = Option.builder("cf")
                .longOpt("configFile")
                .desc("The configuration file to use")
                .required()
                .hasArg(true)
                .build();
        options.addOption(option);
        org.apache.commons.cli.CommandLine cmd = null;
        try {
            cmd = (new DefaultParser()).parse(options, args);
        } catch (ParseException e) {
            log.error(e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java -jar xmi-codegen.jar", options);
            System.exit(1);
        }
        return cmd;
    }
}
