package code.utils;

import code.exceptions.GenException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Serialization {
    private static final Logger log = LogManager.getLogger(Serialization.class);
    private static final ObjectMapper mapper = new ObjectMapper();

    private Serialization() {
        // Static class
    }

    public static String toJson(Object object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            throw new GenException(e.getMessage());
        }
    }
}
