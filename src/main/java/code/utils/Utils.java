package code.utils;

import code.exceptions.GenException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;

public class Utils {
    private final static Logger log = LogManager.getLogger(Utils.class);

    public static void validateFileExistence(String filePath) {
        File file = new File(filePath);
        if (!file.exists() || file.isDirectory()) {
            String errorMessage = String.format("Not a file: %s", filePath);
            log.error(errorMessage);
            throw new GenException(errorMessage);
        }
    }
}
