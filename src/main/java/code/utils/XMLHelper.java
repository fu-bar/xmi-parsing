package code.utils;

import code.exceptions.GenException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XMLHelper {
    private final Logger log = LogManager.getLogger(XMLHelper.class);
    private final XPath xpath = XPathFactory.newInstance().newXPath();

    public String getAttribute(Node node, String attribute) {
        String nodeValue = node.getAttributes().getNamedItem(attribute).getNodeValue();
        if (nodeValue == null) {
            String errorMessage = String.format("Failed getting attribute %s", attribute);
            log.error(errorMessage);
            throw new GenException(errorMessage);
        }
        return nodeValue;
    }

    public Document readFile(String xmlFilePath) {

        Utils.validateFileExistence(xmlFilePath);
        File file = new File(xmlFilePath);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, ""); // Set due to sonar advice
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, ""); // Set due to sonar advice
        factory.setNamespaceAware(false);

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            log.debug(String.format("Reading %s", xmlFilePath));
            Document parsedDocument = builder.parse(new FileInputStream(file));
            log.debug(String.format("Finishes reading %s", xmlFilePath));
            return parsedDocument;
        } catch (ParserConfigurationException | SAXException | IOException e) {
            String errorMessage = e.getMessage();
            log.error(errorMessage);
            throw new GenException(errorMessage, e);
        }
    }

    public List<Node> toList(NodeList nodeList) {
        List<Node> list = new ArrayList<>();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nNode = nodeList.item(i);
            list.add(nNode);
        }
        return list;
    }

    public List<Node> fetchXpath(Node root, String xpathExpression) {
        try {
            NodeList nodeList = (NodeList) xpath.compile(xpathExpression).evaluate(root, XPathConstants.NODESET);
            return toList(nodeList);
        } catch (XPathExpressionException e) {
            log.error(e.getMessage(), e);
            throw new GenException(e.getMessage());
        }
    }
}
