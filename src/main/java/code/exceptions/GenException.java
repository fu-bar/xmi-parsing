package code.exceptions;

public class GenException extends RuntimeException {

    public GenException(String errorMessage) {
        super(errorMessage);
    }

    public GenException(String errorMessage, Throwable cause) {
        super(errorMessage, cause);
    }
}
