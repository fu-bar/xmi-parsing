package code;

import code.blocks.FieldInfo;
import code.blocks.TypeInfo;
import code.blocks.XMIParser;
import code.config.Config;
import code.exceptions.GenException;
import code.utils.TypeResolver;
import code.utils.Utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.javapoet.*;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import osf.modules.elm.OSFImmutableEntity;
import osf.objects.annotations.FieldMetadata;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static code.utils.CommandLineHelper.parseCommandLine;

// TODO: Handle collections
// TODO: Handle detailed annotations
// TODO: Handle indexes
// TODO: Get generated package from XMI
// TODO: Add references constraint annotations
// TODO: Generate additional classes AS-IS (but as read-only)
public class Generator {
    private static final String SAMPLE_FILE_PATH = "c:\\workspace\\xslt\\xmi\\level2.xml";
    private static final String SAMPLE_TARGET_FOLDER = "c:\\temp";
    private static final Logger log = LogManager.getLogger(Generator.class);
    private static final String CREATION_TOKEN = "creationToken";
    private static TypeResolver typeResolver;

    public static void main(String[] args) {
        Options options = new Options();
        CommandLine cmd = parseCommandLine(args, options);
        String xmiFilePath = cmd.getOptionValue("xmiFilePath");
        String configFilePath = cmd.getOptionValue("configFile");
        String targetFolder = cmd.getOptionValue("targetFolder");
        String packageFQN = cmd.getOptionValue("packageFQN");


        Utils.validateFileExistence(configFilePath);
        ObjectMapper mapper = new ObjectMapper();
        try {
            Config config = mapper.readValue(new File(configFilePath), Config.class);
            typeResolver = new TypeResolver(config);
        } catch (IOException e) {
            String errorMessage = "Failed reading configuration file ()";
            log.error(errorMessage, e);
            System.exit(1);
        }

        Arrays.asList(xmiFilePath, configFilePath, targetFolder, packageFQN).forEach(
                param -> {
                    if (param == null) {
                        HelpFormatter formatter = new HelpFormatter();
                        formatter.printHelp("java -jar xmi-codegen.jar", options);
                        System.exit(1);
                    }
                });

        XMIParser xmiParser = new XMIParser();
        List<TypeInfo> classes = xmiParser.parseXML(SAMPLE_FILE_PATH);
        log.debug(String.format("Collected information on %d classes", classes.size()));

        Generator generator = new Generator();
        log.debug(String.format("Files will be generated under %s", SAMPLE_TARGET_FOLDER));
        classes.forEach(typeInfo -> generator.generate(typeInfo, SAMPLE_TARGET_FOLDER, packageFQN));
        log.info("Done :-)");
    }


    private void generate(TypeInfo typeInfo, String targetFolder, String packageFQN) {
        boolean folderCreated = new File(targetFolder).mkdir();// Create target folder if does not exist
        if (folderCreated) {
            log.info(String.format("Folder created: %s", targetFolder));
        }
        String generatedClassName = typeInfo.getName();

        TypeSpec.Builder immutable = TypeSpec.classBuilder(generatedClassName).addModifiers(Modifier.PUBLIC);
        TypeSpec.Builder writable = TypeSpec.classBuilder(String.format("%sWritable", generatedClassName)).addModifiers(Modifier.PUBLIC);
        List<TypeSpec.Builder> bothClasses = Arrays.asList(immutable, writable);

        if (typeInfo.getInherits().isEmpty()) {
            immutable.superclass(OSFImmutableEntity.class);
        } else {
            immutable.superclass(typeResolver.resolve(typeInfo.getInherits().get()));
        }

        List<FieldInfo> fieldDetails = typeInfo.getFieldInfos();
        List<FieldSpec> privateFields = buildPrivateFields(fieldDetails);

        privateFields.forEach(privateField -> bothClasses.forEach(clazz -> clazz.addField(privateField)));
        bothClasses.forEach(clazz -> clazz.addMethods(buildGetters(privateFields)));
        writable.addMethods(buildSetters(privateFields));
        writable.addMethods(buildWritableConstructors(writable.build(), privateFields, packageFQN));
        immutable.addMethods(buildImmutableConstructors(immutable.build(), privateFields, packageFQN));

        writeToFile(targetFolder, packageFQN, writable.build());
        writeToFile(targetFolder, packageFQN, immutable.build());
    }

    private List<FieldSpec> buildPrivateFields(List<FieldInfo> fieldInfos) {
        List<FieldSpec> privateFields = new ArrayList<>();
        fieldInfos.forEach(fieldInfo -> {
            Class<?> fieldType = typeResolver.resolve(fieldInfo.getFieldType());
            FieldSpec.Builder field = FieldSpec.builder(fieldType, fieldInfo.getName())
                    .addModifiers(Modifier.PRIVATE, Modifier.FINAL); // TODO: Consider when to remove the final modifier

            List<String> annotations = fieldInfo.getFieldAnnotations();
            for (String annotation : annotations) {
                field.addAnnotation(typeResolver.resolve(annotation));
            }
            AnnotationSpec.Builder fieldMetadata = AnnotationSpec.builder(FieldMetadata.class);
            fieldInfo.getDetailedAnnotations().forEach((key, value) -> {
                fieldMetadata.addMember(key, "$S", value); // TODO: This may not always be a string
            });
            if (!fieldMetadata.members.isEmpty()) {
                field.addAnnotation(fieldMetadata.build());
            }
            privateFields.add(field.build());
        });
        return privateFields;
    }

    private List<MethodSpec> buildGetters(List<FieldSpec> fieldSpecs) {
        List<MethodSpec> getters = new ArrayList<>();
        fieldSpecs.forEach(fieldSpec -> {
            String fieldName = fieldSpec.name;
            String getterName = String.format("get%s", fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1));
            MethodSpec getter = MethodSpec.methodBuilder(getterName)
                    .addModifiers(Modifier.PUBLIC)
                    .returns(fieldSpec.type)
                    .addStatement(String.format("return this.%s", fieldName))
                    .build();
            getters.add(getter);
        });
        return getters;
    }

    private List<MethodSpec> buildSetters(List<FieldSpec> fieldSpecs) {
        List<MethodSpec> getters = new ArrayList<>();
        fieldSpecs.forEach(fieldSpec -> {
            String fieldName = fieldSpec.name;
            String getterName = String.format("set%s", fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1));
            MethodSpec getter = MethodSpec.methodBuilder(getterName)
                    .addModifiers(Modifier.PUBLIC)
                    .returns(void.class)
                    .addParameter(fieldSpec.type, "value")
                    .addStatement(String.format("this.%s = %s", fieldName, "value"))
                    .build();
            getters.add(getter);
        });
        return getters;
    }

    private List<MethodSpec> buildWritableConstructors(TypeSpec typeSpec, List<FieldSpec> fieldSpecs, String packageFQN) {
        List<MethodSpec> constructors = new ArrayList<>();
        MethodSpec defaultConstructor = MethodSpec.methodBuilder(typeSpec.name)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(UUID.class, CREATION_TOKEN)
                .addStatement("super(creationToken)")
                .build();
        constructors.add(defaultConstructor);


        ClassName copyConstructorClassName = ClassName.get("", typeSpec.name);
        MethodSpec.Builder copyConstructor = MethodSpec.methodBuilder(typeSpec.name)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(UUID.class, CREATION_TOKEN)
                .addParameter(copyConstructorClassName, "source")
                .addStatement("super(creationToken)");
        fieldSpecs.forEach(fieldSpec -> copyConstructor.addStatement(setterStatementFor(fieldSpec)));

        constructors.add(copyConstructor.build());

        ClassName copyConstructorFromImmutableClassName = ClassName.get(packageFQN, typeSpec.name.replace("Writable", ""));
        MethodSpec.Builder fromImmutableCopyConstructor = MethodSpec.methodBuilder(typeSpec.name)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(UUID.class, CREATION_TOKEN)
                .addParameter(copyConstructorFromImmutableClassName, "source")
                .addStatement("super(creationToken)");

        fieldSpecs.forEach(fieldSpec -> fromImmutableCopyConstructor.addStatement(setterStatementFor(fieldSpec)));

        constructors.add(fromImmutableCopyConstructor.build());

        return constructors;
    }

    private List<MethodSpec> buildImmutableConstructors(TypeSpec typeSpec, List<FieldSpec> fieldSpecs, String packageFQN) {
        List<MethodSpec> constructors = new ArrayList<>();
        ClassName writableEntityClassName = ClassName.get(packageFQN, String.format("%sWritable", typeSpec.name));
        ClassName cloningUtilsClassName = ClassName.get("osf.objects.utilities", "CloningUtils");

        MethodSpec.Builder fromWritableCopyConstructor = MethodSpec.methodBuilder(typeSpec.name)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(UUID.class, CREATION_TOKEN)
                .addParameter(writableEntityClassName, "source")
                .addStatement("super(creationToken)");
        fieldSpecs.forEach(fieldSpec -> {
            String fieldPart = fieldSpec.name.substring(0, 1).toUpperCase() + fieldSpec.name.substring(1);
            fromWritableCopyConstructor.addStatement(String.format("this.%s = $T.deepClone(source.get%s())", fieldSpec.name, fieldPart), cloningUtilsClassName);
        });
        constructors.add(fromWritableCopyConstructor.build());
        return constructors;
    }

    private void writeToFile(String targetFolder, String packageFQN, TypeSpec typeSpec) {
        JavaFile javaFile = JavaFile.builder(packageFQN, typeSpec).build();
        try {
            javaFile.writeTo(new File(targetFolder));
        } catch (IOException e) {
            String errorMessage = String.format("Failed writing to %s", targetFolder);
            log.error(errorMessage);
            throw new GenException(errorMessage);
        }
    }

    private String setterStatementFor(FieldSpec fieldSpec) {
        String fieldPart = fieldSpec.name.substring(0, 1).toUpperCase() + fieldSpec.name.substring(1);
        return String.format("this.set%s(source.get%s())", fieldPart, fieldPart);
    }

}
